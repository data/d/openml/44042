# OpenML dataset: black_friday

https://www.openml.org/d/44042

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark,  
                                  transformed in the same way. This dataset belongs to the "regression on categorical and
                                  numerical features" benchmark. Original description: 
 
Customer purchases on Black Friday

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44042) of an [OpenML dataset](https://www.openml.org/d/44042). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44042/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44042/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44042/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

